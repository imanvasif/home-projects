import React, {Component} from 'react';
import './App.scss';
import Preloader from "./component/Preloader/Preloader";
import ProductList from "./component/ProductList/ProductList";
class App extends Component {
    
    state={data:''}
    
    componentDidMount() {
        fetch('product.json').then(response => response.json())
        .then(data => { this.setState({data: data}); });
        
        localStorage.clear();
        
        localStorage.setItem('carts', JSON.stringify([]));
        
        localStorage.setItem('favorites', JSON.stringify([]));
    }

    render() {
        const data = this.state.data;
        return (
            <div className="container">
                <div className = "cart-area"></div>
                { data.length > 0 ?  <ProductList data={data}/> : <Preloader/> }
            </div>
        );
    }
}

export default App;



