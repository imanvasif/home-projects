import React,{forwardRef,useImperativeHandle} from "react";
import ReactDOM from "react-dom";
import Proptypes from "prop-types";
import './ModalWindow.scss';

const ModalWindow = forwardRef(
    (props,ref) => {
        
        const [show,setShow] = React.useState(false);
        const [data,setData] = React.useState('');
        
        const open = (self) => { 
            setShow(true); 
            setData(self)
        };
        const close = () => { 
            setShow(false) 
        };
        const okButton = () => { 
            props.okButton(data);
            setShow(false) 
        };
        
        useImperativeHandle(ref,()=>{
             return{    openModal: (self) => open(self)  
            } 
        });
        if (show){
            return ReactDOM.createPortal(
                <div className={"modal-wrapper"}>
                    <div onClick={close} className="modal-overlay"/>
                    <div className="modal-content">
                        <div className="modal-head">
                            <h5>{props.head}</h5>
                            <p className = "x" onClick={close}>X</p>
                        </div>
                        <div className="modal-body">
                            <p>{props.text}</p>
                            <div className="modal-button">
                                <button onClick={okButton} className="modal-button-btn">Ok</button><button onClick={close} className="modal-button-btn btn-blue">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                ,document.getElementById("modal"))
        }
        return null;

    }
);
ModalWindow.propTypes={
     head : Proptypes.string.isRequired,
     text : Proptypes.string.isRequired
    }
ModalWindow.propTypes={
     
    }

export default ModalWindow;