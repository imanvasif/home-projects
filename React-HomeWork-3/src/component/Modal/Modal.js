import React,{forwardRef,useImperativeHandle} from "react";
import ReactDOM from "react-dom";
import Proptypes from "prop-types";
import './Modal.scss';

const Modal = forwardRef(
    (props,ref) => {
        
        const [show,setShow] = React.useState(false);
        const [data,setData] = React.useState('');
        
        const open = (self) => { setShow(true); setData(self)};
        const close = () => { setShow(false) };
        
        const okCondition = () => { props.okCondition(data);setShow(false) };
        useImperativeHandle(ref,()=>{
             return{    openModal: (self) => open(self)   }  });
        
        if (show){
            return ReactDOM.createPortal(
                <div className={"modal-wrapper"}>
                    
                    <div onClick={close} className="modal-overlay"/>
                    <div className="modal-content">
                        
                        <div className="modal-head">
                            <h5>{props.head}</h5>
                            <p className = "x" onClick={close}>X</p>
                        </div>
                        
                        <div className="modal-body">
                            <p>{props.text}</p>
                            
                            <div className="modal-button">
                                <button onClick={okCondition} className="modal-button-btn">Ok</button><button onClick={close} className="modal-button-btn btn-blue">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                ,document.getElementById("modal"))
        }
        return null;

    }
);
Modal.propTypes={ head : Proptypes.string }
Modal.propTypes={ text : Proptypes.string }

export default Modal;