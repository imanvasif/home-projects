import React, {useState,useEffect} from 'react';
import './App.scss';
import Header from "./component/Header/Header";
import Preloader from "./component/Preloader/Preloader";
import ProductList from "./component/ProductList/ProductList";
import Footer from "./component/Footer/Footer";

const App =()=> {
    const [data,setData] = useState('');
    
    useEffect(() => {
        if (localStorage.getItem('carts') === null){ localStorage.setItem('carts', JSON.stringify([])); }
        if (localStorage.getItem('favorites') === null){ localStorage.setItem('favorites', JSON.stringify([])); }
        fetch('product.json').then(r => r.json())
            .then((data) => {  setData(data);});
    }, []);
        return (
            <div className="container">
               <div className = "cart-area" >
                <Header/>
                </div>
                { data.length > 0 ?  <ProductList data={data}/> : <Preloader/> }
                <Footer/>
            </div>
        );
}

export default App;



