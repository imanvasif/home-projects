import React from 'react'
import "./ModalWindow.scss";
import PropTypes from 'prop-types';


const ModalWindow = (props) => {
    return(
        <div className = "ModalWindow" style={(props.showWindow) ? {display:"block"} : {display:"none"}} onClick = {props.closeWindow}>
            <div className = "ModalWindow-container">
                <div className = "ModalWindow-header">
                    Do you want to delete this file?
                    <button className = "ModalWindow-cancel" onClick = {props.closeWindow} >X</button>
                </div>
                <div className = "ModalWindow-body">
                    <p>Once you delete this file,  it won't be possible to undo this action.</p>
                    <p>Are you sure you want to delete it?</p>
                </div>

                <div className= "ModalWindow-buttons">
                    <button className = "ModalWindow-but-tons"  onClick = {props.closeWindow} >Ok</button>
                    <button className = "ModalWindow-but-tons"  onClick = {props.closeWindow}  > Cancel</button>
                </div>
            </div>
        </div>
    )
};

    ModalWindow.propTypes = {
    closeWindow: PropTypes.func,
    showWindow: PropTypes.bool
};


export default ModalWindow