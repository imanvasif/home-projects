import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';


const Button = (props) => {
    return (
        <button type={"button"} className={'container-button'} style={{backgroundColor:props.backgroundColor}} onClick={props.OnClickButton}>{props.text}</button>
    );
};
Button.propTypes={
    backgroundColor:PropTypes.string,
    text:PropTypes.string,
    OnClickButton:PropTypes.func
}
export default Button;

