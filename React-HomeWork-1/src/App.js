import React, { Component } from "react";
import Button from "./components/Button/Button";
import ModalWindow from "./components/ModalWindow/ModalWindow";

export default class App extends Component {
  state = {
    isShowButtonContainer: true,
    isShowModalFirst: false,
    isShowModalSecond: false
  };
  OnClickFirstModal = () => {
    this.setState({
      isShowButtonContainer: !this.state.isShowButtonContainer,
      isShowModalFirst: !this.state.isShowModalFirst
    });
  };
  OnClickOutsideFirst = e => {
    if (e.currentTarget === e.target ) {
      this.setState({
        isShowButtonContainer: !this.state.isShowButtonContainer,
        isShowModalFirst: !this.state.isShowModalFirst
      });
    }
  };
  OnClickSecondModal = () => {
    this.setState({
      isShowButtonContainer: !this.state.isShowButtonContainer,
      isShowModalSecond: !this.state.isShowModalSecond
    })
  }
  OnClickOutsideSecond = e => {
    if (e.currentTarget === e.target) {
      this.setState({
        isShowButtonContainer: !this.state.isShowButtonContainer,
        isShowModalSecond: !this.state.isShowModalSecond
      });
    }
  };

  render() {
    return (
      <div className="App">
        <div
          style={
            this.state.isShowButtonContainer
              ? { display: "block" }
              : { display: "none" }
          }
        >
          <Button
            type="button"
            backgroundColor={"red"}
            text={"Open the first modal window"}
            OnClickButton={this.OnClickFirstModal}
          />
          <Button
            type="button"
            backgroundColor={"dodgerblue"}
            text={"Open the second modal window"}
            OnClickButton={this.OnClickSecondModal}
          />
        </div >
        
        <ModalWindow 
          showWindow={this.state.isShowModalFirst}
          closeWindow={this.OnClickOutsideFirst}
          closeButton={this.OnClickFirstModal}
          onClick = {this.OnClickOutsideFirst}
        />
        <ModalWindow
          showWindow={this.state.isShowModalSecond}
          closeWindow={this.OnClickOutsideSecond}
          closeButton={this.OnClickSecondModal}
        />
      </div>
    );
  }
}
